from logging import log
import os
import random
import concurrent.futures
from tqdm import tqdm
import requests
from bs4 import BeautifulSoup
from db_connection import Connection
from constants import sql_query
from log_manager import LogManager

conn = Connection()
logger = LogManager("PhonePhotoScraper", "./logs/phone_photo_scaraper.log")

BASE_URL = 'https://www.phonearena.com/'
TIMEOUT = 10
ALL_UAS = 'https://gist.githubusercontent.com/ozturkoktay/f1073b3038cab632c16231ef73353d7c/raw/cf847b76a142955b1410c8bcef3aabe221a63db1/user-agents.txt'
PROXIES = {}
WORKERS = 100
PHONE_MODELS = conn.db_select(sql_query, [])


def select_random_UAS() -> str:
    r = requests.get(ALL_UAS)
    if r.status_code == 200:
        urls = r.text.split('\n')
    return random.choice(urls)


def create_folder(folder_name: str):
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    return None


def download_phone_images(id: str, model: str) -> None:
    model = ' '.join(list(dict.fromkeys(model.split())))
    url = f'{BASE_URL}search?term="{model}"'
    logger.info(f'Checking {id} image...')
    if not os.path.isfile(f'imgs/{id}.jpg') or os.path.getsize(f'imgs/{id}.jpg') == 0:
        # if not os.path.exists(f'imgs/{id}.jpg'):
        page = requests.get(url, allow_redirects=True, timeout=TIMEOUT,
                            proxies=PROXIES, headers={'User-Agent': select_random_UAS()})
        soup = BeautifulSoup(page.text, 'html.parser')
        # phone_name = soup.select('p.title')[0].text
        try:
            link = soup.select('picture.square > img')[0].get("data-src")
            # filename = os.path.join('imgs/', link.split('/')[-1])
            with open(f"imgs/{id}.jpg", 'wb') as f:
                logger.info(f'Downloading {model}\'s image...')
                f.write(requests.get(link, allow_redirects=True, proxies=PROXIES,
                                     headers={'User-Agent': select_random_UAS()}).content)
        except IndexError:
            logger.warn(f'{model} image not found on the website.')
    else:
        logger.info(f'{model} image already exists.')


def p_download_phone_images():
    with concurrent.futures.ThreadPoolExecutor(WORKERS) as executor:
        for id, model in PHONE_MODELS:
            executor.submit(download_phone_images, id, model)
        """
        for data in tqdm(concurrent.futures.as_completed(res)):
            data = data.result
        """


if __name__ == "__main__":
    logger.info('Starting...')
    create_folder("imgs")
    p_download_phone_images()
    logger.info('Finished.')
