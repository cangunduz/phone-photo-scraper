#!usr/bin/env python
# -*- coding: utf-8 -*-

import mysql.connector
import json


CONFIG = {
    "DATABASE_USER": "OTASS",
    "DATABASE_PASS": "Otass!!357.",
    "DATABASE_NAME": "otass",
    "DATABASE_URL":  "otadb00.mtel.link",
    "DATABASE_PORT": "3306"
}


class Connection:

    def __init__(self):
        self.DBCONNECTION = mysql.connector.connect(
            user=CONFIG['DATABASE_USER'], password=CONFIG['DATABASE_PASS'],
            host=CONFIG['DATABASE_URL'], database=CONFIG['DATABASE_NAME'],
            charset='utf8mb4', use_unicode=True
        )

        self.MYCURSOR = self.DBCONNECTION.cursor()

    def db_execute(self, sql: str, params: tuple):
        """
        Execute given db operation

        :param sql: SQL Statement
        :type sql: str
        :param params: parameters
        :type params: tuple
        """
        self.MYCURSOR.execute(sql, params)

    def db_insert(self, sql: str, params: tuple) -> int:
        """
        Insert data to database

        :param sql: sql statement
        :type sql: str
        :param params: variable parameters
        :type params: tuple
        :return: insert result
        :rtype: int
        """
        return self.MYCURSOR.execute(sql, params)

    def db_multi_insert(self, sql: str, params: tuple) -> int:
        """
        Insert data to database

        :param sql: sql statement
        :type sql: str
        :param params: variable parameters
        :type params: tuple
        :return: insert result
        :rtype: int
        """
        return self.MYCURSOR.executemany(sql, params)

    def db_select(self, sql: str, params: tuple):
        """
        Select and retrieve data from database

        :param sql: sql statement
        :type sql: str
        :param params: variable parameters
        :type params: tuple
        :return: insert result
        :rtype: int
        """
        self.db_execute(sql, params)
        return self.MYCURSOR.fetchall()

    """
    def __del__(self):
        self.DBCONNECTION.close()
        self.MYCURSOR.close()
    """

    def db_select_JSON(self, sql: str, params: tuple, names: tuple):
        """
        Select and retrieve data as JSON

        :param sql: sql statement
        :type sql: str
        :param params: variable parameters
        :type params: tuple
        :return: insert result
        :rtype: int
        """
        rows = self.db_select(sql, params)
        lists = []
        for r in rows:
            result = {i: r[counter] for counter, i in enumerate(names)}
            j_result = json.dumps(
                result, ensure_ascii=False).encode('utf8').decode()
            lists.append((j_result,))
        return lists

    def db_list(self, sql: str, params: tuple = None) -> list:
        rows = self.db_select(sql, params)
        return [r[0] for r in rows]

    def db_dict(self, sql: str, params: tuple = None) -> dict:
        rows = self.db_select(sql, params)
        return {r[0]: r[1] for r in rows}
